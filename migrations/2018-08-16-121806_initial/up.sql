-- Your SQL goes here

create table acetaminophin_dose(
  medication text not null primary key,
  mg integer not null);

insert into acetaminophin_dose values ("Vicodin", 325);
insert into acetaminophin_dose values ("Tylenol", 325);
insert into acetaminophin_dose values ("Extra-Strength Tylenol", 500);

create table doses(
  medication text not null,
  dose integer not null,
  ts timestamp not null primary key);
