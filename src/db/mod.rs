extern crate r2d2;

use actix::prelude::*;
use actix_web::error::{Error, ErrorInternalServerError};
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};

pub mod models;
pub mod schema;

use self::models::*;

pub struct MedData {
    pool : Pool<ConnectionManager<SqliteConnection>>
}

impl MedData {
    pub fn new(db_path : &str) -> MedData {
        MedData {
            pool: r2d2::Pool::builder()
                .build(ConnectionManager::<SqliteConnection>::new(db_path))
                .unwrap()
        }
    }
}

impl Actor for MedData {
    type Context = SyncContext<Self>;
}

pub struct ListMedications();
impl Message for ListMedications {
    type Result = Result<Vec<AcetaminophinDose>, Error>;
}

impl Handler<ListMedications> for MedData {
    type Result = <ListMedications as Message>::Result;

    fn handle(&mut self, _msg: ListMedications, _ctx: &mut Self::Context)
    -> Self::Result {
        use self::schema::acetaminophin_dose::dsl::*;

        match self.pool.get() {
            Err(_) => Err(ErrorInternalServerError(
                    "Database connection timeout")),
            Ok(ref conn) =>
                acetaminophin_dose.load::<AcetaminophinDose>(conn)
                .map_err(|e| ErrorInternalServerError(e.to_string())),
        }
    }
}
