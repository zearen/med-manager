use chrono::naive::NaiveDateTime;

use super::schema::*;

#[derive(Queryable)]
pub struct AcetaminophinDose {
    pub name: String,
    pub mg: i32,
}

#[derive(Queryable)]
pub struct Dose {
    pub medication: String,
    pub dose: i32,
    pub ts: NaiveDateTime,
}

#[derive(Insertable)]
#[table_name="doses"]
pub struct AddDose<'a> {
    pub medication: &'a str,
    pub dose: i32,
    pub ts: NaiveDateTime,
}
