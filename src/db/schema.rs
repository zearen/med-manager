table! {
    acetaminophin_dose (medication) {
        medication -> Text,
        mg -> Integer,
    }
}

table! {
    doses (ts) {
        medication -> Text,
        dose -> Integer,
        ts -> Timestamp,
    }
}

allow_tables_to_appear_in_same_query!(
    acetaminophin_dose,
    doses,
);
