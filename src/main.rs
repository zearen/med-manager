#![feature(proc_macro_non_items)]

extern crate actix;
extern crate actix_web;
extern crate chrono;
#[macro_use]
extern crate diesel;
extern crate futures;
extern crate maud;

use actix::dev::{
    Destination, MessageDestination, MessageDestinationTransport, ToEnvelope
};
use actix::prelude::*;
use actix_web::{
    http, server, App, Error, FutureResponse, HttpResponse, State
};
use actix_web::error::ErrorInternalServerError;
use futures::future::Future;
use maud::html;

mod db;
mod settings;

use settings::*;
use db::models::*;

struct MedManagerState{
    med_data : Addr<Syn, db::MedData>,
}

fn render_index(meds: Vec<AcetaminophinDose>) -> maud::Markup {
    html!(
        p { "Known meds:" }
        ul {
            @for med in &meds {
                li {
                    ul {
                        li { (med.name) }
                        li { (med.mg) " mg" }
                    }
                }
            }
        }
    )
}

fn render_or_500<R, F>(response_result: Result<R, Error>, f: F)
    -> HttpResponse
    where F: FnOnce(R) -> maud::Markup {
    match response_result {
        Ok(response) => HttpResponse::Ok()
            .body(f(response).into_string()),
        Err(err) => HttpResponse::from_error(err),
    }
}

fn send_message_then_render<A, S, M, R, F>(
    addr: &Addr<S, A>, message: M, render: F)
    -> FutureResponse<HttpResponse>
    where
        A: Handler<M>,
        <A as Actor>::Context: ToEnvelope<S, A, M>,
        S: MessageDestination<A, M> + 'static,
        <S as Destination<A>>::Transport: MessageDestinationTransport<S, A, M>,
        R: 'static + Send,
        M: 'static,
        M: Message<Result=Result<R, Error>> + Send,
        F: FnOnce(R) -> maud::Markup + 'static {
    Box::new(addr
        .send(message)
        .or_else(|e| Err(ErrorInternalServerError(e.to_string())))
        .map(|res| render_or_500(res, render)))
}

fn index(state: State<MedManagerState>) -> FutureResponse<HttpResponse> {
    send_message_then_render(
        &state.med_data, db::ListMedications(), render_index)
}

fn med_manager_app(settings : AppSettings) -> App<MedManagerState> {
    App::with_state(MedManagerState {
            med_data: SyncArbiter::start(
                          settings.db_threads,
                          move || db::MedData::new(&settings.db_path.clone()))
        })
        .route("/", http::Method::GET, index)
}

fn main() {
    let sys = actix::System::new("med-manager");
    let Settings{app_settings, server_settings} = read_settings();
    
    server::new(move || med_manager_app(app_settings.clone()))
        .bind(server_settings.bind_to)
        .unwrap()
        .start();

    let _ = sys.run();
}
