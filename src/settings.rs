#[derive(Clone)]
pub struct AppSettings {
    pub db_path : String,
    pub db_threads: usize,
}

#[derive(Clone)]
pub struct ServerSettings {
    pub bind_to : String,
}

#[derive(Clone)]
pub struct Settings {
    pub app_settings: AppSettings,
    pub server_settings: ServerSettings,
}

pub fn read_settings() -> Settings {
    // TODO(zearen): Parse this from flags or something.
    Settings {
        app_settings: AppSettings {
            db_path: "meds.db".to_owned(),
            db_threads: 4,
        },
        server_settings: ServerSettings {
            bind_to: String::from("127.0.0.1:8080"),
        },
    }
}
